# Models

* anacat: use substrate for all species?
* implement stoichiometry matrix approach

# Parametrization

* develop strategy to parameterize a growth model systematically
    + collect more parameters
    + stable steady states over realistic dilution rates

# Data Fits

* ODE wrappers for fitting functions in growthrates and FME
    + map modelled to measured values 

# Solver Backend

* see [stochasticlifestyle blog entry](http://www.stochasticlifestyle.com/comparison-differential-equation-solver-suites-matlab-r-julia-python-c-fortran/) for a recent comparison of different solvers
* implement SOSlib-based or -derived interface to sundials and SBML
