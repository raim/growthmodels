# Ordinary differential equation (ODE) models of bacterial growth

For use with R packages [deSolve](https://cran.r-project.org/web/packages/deSolve/index.html) for simulation, and the fitting functions in Thomas Petzold's
R package [growthrates](https://github.com/tpetzoldt/growthrates/), or
the [FME package](https://cran.r-project.org/web/packages/FME/index.html)
for more complex `inverse problems`.

## Installation

```r
library(devtools)
install_git('https://gitlab.com/raim/growthmodels.git', subdir = "pkg", quiet = FALSE)
```

## Details

Base models are formulated without units, but parameters sets
and model wrappers interpret the models with certain unit sets,
and allow to map modelled variables to measured data.

For example, a model's concentrations are interpreted as
C-mol for carbonc substrate and biomass and mol for other
substances. And C-mol biomass can be mapped to cell numbers,
or OD via wrappers and appropriate parameter sets.

The user has to take care of correct parameters and interpretations.

TODO: add parameters for yeast and e.coli, carbon content,
cell volume, OD per cell number, etc.
